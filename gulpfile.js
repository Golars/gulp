var gulp = require('gulp'),
    concat = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    eol = require('gulp-eol');

gulp.task('default', function() {
});

/*** SASS ***/
gulp.task('sass', ['main', 'news']);

/*** SASS ***/
gulp.task('main', function () {
    return gulp.src(['sass/main.sass'])
        .pipe(sass())
        .pipe(concatCss('main.css'))
        .pipe(eol('\n'))
        .pipe(gulp.dest('css/'));
});

/*** SASS ***/
gulp.task('news', function () {
    return gulp.src(['sass/news.sass'])
        .pipe(sass())
        .pipe(concatCss('news.css'))
        .pipe(eol('\n'))
        .pipe(gulp.dest('css/'));
});

/*** SASS ***/
gulp.task('build', ['sass'], function () {
    return gulp.src(['css/main.css'])
        .pipe(concatCss('build.min.css'))
        .pipe(cleanCSS())
        .pipe(minifyCss())
        .pipe(eol('\n'))
        .pipe(gulp.dest('css/'));
});

/*** WATCH ***/
gulp.task('watch', function () {
    gulp.watch('sass/**/*.sass', ['sass']);
});